<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $rows: An array of row items. Each row is an array of content
 *   keyed by field ID.
 * - $header: an array of headers(labels) for fields.
 * - $themed_rows: a array of rows with themed fields.
 * @ingroup views_templates
 */
?>
<?php foreach ($avito_rows as $row): ?>
<Ad>
  <Id><?php print $row['Id'] ?></Id>
  <Category><?php print $row['Category'] ?></Category>
  <TypeId><?php print $row['TypeId'] ?></TypeId>
  <ContactPhone><?php print $row['ContactPhone'] ?></ContactPhone>
  <Region><?php print $row['Region'] ?></Region>
  <Subway><?php print $row['Subway'] ?></Subway>
  <Title><?php print $row['Title'] ?></Title>
  <Description><?php print $row['Description'] ?></Description>
  <Price><?php print $row['Price'] ?></Price>
  <Images>
  <?php foreach ($row['Images'] as $image): ?>
    <Image url="<?php print $image ?>" />
  <?php endforeach; ?>
  </Images>
</Ad>
<?php endforeach; ?>
